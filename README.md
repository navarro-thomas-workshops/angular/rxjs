# RxJS workshop

This project is a quick workshop on RxJS.

You will have an overview of what is possible to do with this lib.

## Subjects
A Subject is an multicasting Observable. Observable are meant to be one to one conversation. Subjects allow you to have a central data source from which you will push data to multiple subscribers.

The learnrxjs comparison is nice :

`"You can think of this as a single speaker talking at a microphone in a room full of people. Their message (the subject) is being delivered to many (multicast) people (the observers) at once."`

Go into the subjects module to learn more.

## Operators
Go into the operators module to learn more.

## Sources
https://www.learnrxjs.io/