import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AsyncSubject } from 'rxjs';

@Component({
  selector: 'app-async-subject',
  templateUrl: './async-subject.component.html',
  styleUrls: ['./async-subject.component.scss']
})
export class AsyncSubjectComponent implements OnInit {

  /**
   * Logs attributes.
   */
  displayer1Logs: Array<string> = [];
  displayer2Logs: Array<string> = [];
  text: string = '';

  textSubject: AsyncSubject<string> = new AsyncSubject();

  displayer1Sub: Subscription = new Subscription;
  displayer2Sub: Subscription = new Subscription;

  constructor() { }

  ngOnInit(): void {
  }

  subscribeDisplayer1(): void {
    this.displayer1Sub = this.textSubject.subscribe((t) => {
      this.displayer1Logs.push(t);
    });
  }

  unsubscribeDisplayer1(): void {
    this.displayer1Sub.unsubscribe();
  }

  subscribeDisplayer2(): void {
    this.displayer2Sub = this.textSubject.subscribe((t) => {
      this.displayer2Logs.push(t);
    });
  }

  unsubscribeDisplayer2(): void {
    this.displayer2Sub.unsubscribe();
  }

  appendText(): void {
    this.textSubject.next(this.text); // We emit the current value.
    this.text = ''; // We reset the value.
  }

  complete(): void {
    this.textSubject.complete();
  }
}
