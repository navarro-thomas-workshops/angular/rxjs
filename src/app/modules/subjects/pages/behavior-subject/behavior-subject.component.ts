import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

@Component({
  selector: 'app-behavior-subject',
  templateUrl: './behavior-subject.component.html',
  styleUrls: ['./behavior-subject.component.scss']
})
export class BehaviorSubjectComponent implements OnInit {

  /**
   * Logs attributes.
   */
  displayer1Logs: Array<string> = [];
  displayer2Logs: Array<string> = [];
  text: string = '';

  textSubject: BehaviorSubject<string> = new BehaviorSubject('Hello');

  displayer1Sub: Subscription = new Subscription;
  displayer2Sub: Subscription = new Subscription;

  constructor() { }

  ngOnInit(): void {
  }

  subscribeDisplayer1(): void {
    this.displayer1Sub = this.textSubject.subscribe((t) => {
      this.displayer1Logs.push(t);
    });
  }

  unsubscribeDisplayer1(): void {
    this.displayer1Sub.unsubscribe();
  }

  subscribeDisplayer2(): void {
    this.displayer2Sub = this.textSubject.subscribe((t) => {
      this.displayer2Logs.push(t);
    });
  }

  unsubscribeDisplayer2(): void {
    this.displayer2Sub.unsubscribe();
  }

  appendText(): void {
    this.textSubject.next(this.text); // We emit the current value.
    this.text = ''; // We reset the value.
  }
}
