import { Component, OnInit } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit {
  
  /**
   * Logs attributes.
   */
  displayer1Logs: Array<string> = [];
  displayer2Logs: Array<string> = [];
  text: string = '';

  textSubject: Subject<string> = new Subject();

  displayer1Sub: Subscription = new Subscription;
  displayer2Sub: Subscription = new Subscription;

  constructor() { }

  ngOnInit(): void {
  }

  subscribeDisplayer1(): void {
    this.displayer1Sub = this.textSubject.subscribe((t) => {
      this.displayer1Logs.push(t);
    });
  }

  unsubscribeDisplayer1(): void {
    this.displayer1Sub.unsubscribe();
  }

  subscribeDisplayer2(): void {
    this.displayer2Sub = this.textSubject.subscribe((t) => {
      this.displayer2Logs.push(t);
    });
  }

  unsubscribeDisplayer2(): void {
    this.displayer2Sub.unsubscribe();
  }

  appendText(): void {
    this.textSubject.next(this.text); // We emit the current value.
    this.text = ''; // We reset the value.
  }
}
