import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-replay-subject',
  templateUrl: './replay-subject.component.html',
  styleUrls: ['./replay-subject.component.scss']
})
export class ReplaySubjectComponent implements OnInit {

  /**
   * Logs attributes.
   */
  displayer1Logs: Array<string> = [];
  displayer2Logs: Array<string> = [];
  text: string = '';

  textSubject: ReplaySubject<string> = new ReplaySubject();

  displayer1Sub: Subscription = new Subscription;
  displayer2Sub: Subscription = new Subscription;

  constructor() { }

  ngOnInit(): void {
  }

  subscribeDisplayer1(): void {
    this.displayer1Sub = this.textSubject.subscribe((t) => {
      this.displayer1Logs.push(t);
    });
  }

  unsubscribeDisplayer1(): void {
    this.displayer1Sub.unsubscribe();
  }

  subscribeDisplayer2(): void {
    this.displayer2Sub = this.textSubject.subscribe((t) => {
      this.displayer2Logs.push(t);
    });
  }

  unsubscribeDisplayer2(): void {
    this.displayer2Sub.unsubscribe();
  }

  appendText(): void {
    this.textSubject.next(this.text); // We emit the current value.
    this.text = ''; // We reset the value.
  }
}
