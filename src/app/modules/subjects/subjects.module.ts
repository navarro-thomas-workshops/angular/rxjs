import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubjectComponent } from './pages/subject/subject.component';
import { AsyncSubjectComponent } from './pages/async-subject/async-subject.component';
import { BehaviorSubjectComponent } from './pages/behavior-subject/behavior-subject.component';
import { ReplaySubjectComponent } from './pages/replay-subject/replay-subject.component';
import { SubjectsRoutingModule } from './subjects-routing.module';
import { SharedModule } from 'rxjs-shared/shared.module';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [SubjectComponent, AsyncSubjectComponent, BehaviorSubjectComponent, ReplaySubjectComponent],
  imports: [
    CommonModule,
    SubjectsRoutingModule,
    SharedModule,
    MatInputModule,
    MatIconModule,
    FormsModule,
    FlexModule,
    FlexLayoutModule,
    MatButtonModule
  ]
})
export class SubjectsModule { }
