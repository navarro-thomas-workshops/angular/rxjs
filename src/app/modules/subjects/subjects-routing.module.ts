import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubjectsHomeComponent } from 'rxjs-shared/components';
import { RouterConstant } from 'rxjs-shared/constants';
import { AsyncSubjectComponent } from './pages/async-subject/async-subject.component';
import { BehaviorSubjectComponent } from './pages/behavior-subject/behavior-subject.component';
import { ReplaySubjectComponent } from './pages/replay-subject/replay-subject.component';
import { SubjectComponent } from './pages/subject/subject.component';

const routes: Routes = [
    { path: '', component: SubjectsHomeComponent },
    { path: RouterConstant.SUBJECTS_SUBJECT, component: SubjectComponent },
    { path: RouterConstant.SUBJECTS_ASYNC_SUBJECT, component: AsyncSubjectComponent },
    { path: RouterConstant.SUBJECTS_BEHAVIOR_SUBJECT, component: BehaviorSubjectComponent },
    { path: RouterConstant.SUBJECTS_REPLAY_SUBJECT, component: ReplaySubjectComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SubjectsRoutingModule { }
