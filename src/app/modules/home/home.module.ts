import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { MatDividerModule } from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';
import { SharedModule } from 'rxjs-shared/shared.module';




@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    MatDividerModule,
    MatCardModule,
    FlexLayoutModule,
    FlexModule
  ]
})
export class HomeModule { }
