import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OperatorsRoutingModule } from './operators-routing.module';
import { CreationComponent } from './pages/creation/creation.component';
import { SharedModule } from 'rxjs-shared/shared.module';


@NgModule({
  declarations: [CreationComponent],
  imports: [
    CommonModule,
    OperatorsRoutingModule,
    SharedModule
  ]
})
export class OperatorsModule { }
