import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperatorsHomeComponent } from 'rxjs-shared/components';
import { RouterConstant } from 'rxjs-shared/constants';
import { CreationComponent } from './pages/creation/creation.component';


const routes: Routes = [{ path: '', component: OperatorsHomeComponent },
          { path: RouterConstant.CREATION, component: CreationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperatorsRoutingModule { }
