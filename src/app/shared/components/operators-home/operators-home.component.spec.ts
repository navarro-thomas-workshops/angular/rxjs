import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OperatorsHomeComponent } from './operators-home.component';

describe('OperatorsHomeComponent', () => {
  let component: OperatorsHomeComponent;
  let fixture: ComponentFixture<OperatorsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OperatorsHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatorsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
