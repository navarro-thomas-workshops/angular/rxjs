/**
 * Constants for routes.
 */
export class RouterConstant {

    /**************************************************************
     ********************** The Home module. **********************
     **************************************************************/
    public static HOME = 'home';

    /**************************************************************
     ******************** The Subjects module. ********************
     **************************************************************/
    public static SUBJECTS = 'subjects';

    /**
     * The subject page.
     */
    public static SUBJECTS_SUBJECT = 'subject';

    /**
     * The async subject page.
     */
    public static SUBJECTS_ASYNC_SUBJECT = 'async-subject';

    /**
     * The behavior subject page.
     */
    public static SUBJECTS_BEHAVIOR_SUBJECT = 'behavior-subject';

    /**
     * The replay subject page.
     */
    public static SUBJECTS_REPLAY_SUBJECT = 'replay-subject';

    /**************************************************************
     ******************* The Operators module. ********************
     **************************************************************/
    public static OPERATORS = 'operators';

    /**
     * The creation operators page.
     */
    public static CREATION = 'creation';
}