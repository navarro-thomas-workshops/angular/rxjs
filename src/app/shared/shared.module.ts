import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';
import { SubjectsHomeComponent, OperatorsHomeComponent, DisplayerComponent } from './components';
import { MatCardModule } from '@angular/material/card';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [DisplayerComponent, SubjectsHomeComponent, OperatorsHomeComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FlexModule,
    MatCardModule,
    RouterModule
  ],
  exports: [DisplayerComponent, SubjectsHomeComponent, OperatorsHomeComponent]
})
export class SharedModule { }
