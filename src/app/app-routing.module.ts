import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouterConstant } from 'rxjs-shared/constants';

const routes: Routes = [
  { path: '', loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule) },
  { path: RouterConstant.HOME, loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule) },
  { path: RouterConstant.SUBJECTS, loadChildren: () => import('./modules/subjects/subjects.module').then(m => m.SubjectsModule) },
  { path: RouterConstant.OPERATORS, loadChildren: () => import('./modules/operators/operators.module').then(m => m.OperatorsModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
